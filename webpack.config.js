const webpack = require('webpack')
const path = require('path')

module.exports = {
  context: __dirname,
  entry: './js/src/app.js',
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, './js/dist/')
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loaders: ['babel-loader']
      }, {
        test: /\.css$/i,
        exclude: /node_modules/,
        loaders: [ 'style-loader', 'css-loader' ],
      },
    ]
  }
}
