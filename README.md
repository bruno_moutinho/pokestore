# POKÉSTORE

## INSTALL
```
$ npm install
```

## RUN PROJECT
```
$ npm run build
$ npm start
```

## RUN WATCH PROJECT
### Terminal 1
```
$ npm run build:watch
```

### Terminal 2
```
$ npm start
```