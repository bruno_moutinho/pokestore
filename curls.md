# FETCH ALL POKEMON
curl 'localhost:3000/pokemon'

# CREATE NEW POKEMON
curl 'localhost:3000/pokemon' \
	-H "Content-Type: application/json" \
	-d '{"name": "Bulbasaur", "number": 1, "price": 100, "stock": 10}'
curl 'localhost:3000/pokemon' \
	-H "Content-Type: application/json" \
	-d '{"name": "Ivysaur", "number": 2, "price": 400, "stock": 10}'
curl 'localhost:3000/pokemon' \
	-H "Content-Type: application/json" \
	-d '{"name": "Venusaur", "number": 3, "price": 1000, "stock": 10}'
curl 'localhost:3000/pokemon' \
	-H "Content-Type: application/json" \
	-d '{"name": "Charmander", "number": 4, "price": 100, "stock": 10}'
curl 'localhost:3000/pokemon' \
	-H "Content-Type: application/json" \
	-d '{"name": "Charmeleon", "number": 5, "price": 400, "stock": 10}'
curl 'localhost:3000/pokemon' \
	-H "Content-Type: application/json" \
	-d '{"name": "Charizard", "number": 6, "price": 1000, "stock": 10}'
curl 'localhost:3000/pokemon' \
	-H "Content-Type: application/json" \
	-d '{"name": "Squirtle", "number": 7, "price": 100, "stock": 10}'
curl 'localhost:3000/pokemon' \
	-H "Content-Type: application/json" \
	-d '{"name": "Wartortle", "number": 8, "price": 400, "stock": 10}'
curl 'localhost:3000/pokemon' \
	-H "Content-Type: application/json" \
	-d '{"name": "Blastoise", "number": 9, "price": 1000, "stock": 10}'

# BUY A POKEMON
curl 'localhost:3000/store/pokemon' \
	-H "Content-Type: application/json" \
	-d '{"name": "Bulbasaur", "quantity": 1}'