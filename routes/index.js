const path = require('path')
const router = require('express').Router()
const Pokemon = require('../models/pokemon-model');
const Store = require('../store')

router.route('/pokemon')
	.get((req, res) => {
		Pokemon.findAll()
			.then(function listOfPokemons(pokemons) {
				res.send(pokemons);
			})
	})
	.post((req, res) => {
		Pokemon.findOrCreate({
			where: {
				number: req.body.number
			},
			defaults: {
				name: req.body.name,
				price: req.body.price,
				stock: req.body.stock
			}
		})
			.spread((pokemon, created) => {
				if (!created) {
					pokemon.stock += req.body.stock
					pokemon.save()
				}
				res.send(pokemon)
			})
	})
	.put((req, res) => {
		Pokemon.findOne({ where: { number: req.body.number } })
		.then(pokemon => {
			pokemon.price = req.body.price
		})
	})

router.route('/store/:product')
	.post(async (req, res) => {
		const store = Store(req.params.product)
		if (!store) {
			return res.status(400).send({
				error: `The store for ${req.params.product} does not exist`
			})
		}
		let response = {}
		const { cart } = req.body
		console.log('cart', cart)
		for (product of cart) {
			let productResponse = {}
			const productStore = await store(product.name)

			if (!productStore.hasInStock(product.quantity)) {
				productResponse = Object.assign({}, productResponse, {
					error: `There is not enough ${productStore.product} in ${productStore.name} for you. We currently have ${productStore.stock} units of ${productStore.product}.`
				})
			} else {
				productResponse = await productStore.sell(product.quantity)
					.then(res => {
						if (res.ok) {
							return Object.assign({}, productResponse, { succcess: res.body.success })
						} else {
							return Object.assign({}, productResponse, { error: res.body.errors })
						}
					})
					.catch(err =>  Object.assign({}, productResponse, { error: err }))
			}
			response[product.name] = productResponse
		}
		res.send(response)
	})

router.route('/')
	.get((req, res) => {
		res.sendFile(path.join(__dirname, '../app/index.html'))
	})

router.route('/js/:file')
	.get((req, res) => {
		res.sendFile(path.join(__dirname, `../js/dist/${req.params.file}`))
	})

module.exports = router