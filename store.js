const Pokemon = require('./models/pokemon-model')
const PaymentGateway = require('./payment/payment_gateway')()

const Store = product => {
	switch( product ) {
		case 'pokemon':
			return PokemonStore
		default:
			return
	}
}

const PokemonStore = async pokemonName => {
	const pokemon = await Pokemon.findOne({
		where: {
			name: pokemonName
		}
	})
	return {
		name: 'PokeStore',
		product: pokemonName,
		stock: pokemon.stock,
		hasInStock: quantity => pokemon.stock > quantity,
		sell: quantity => PaymentGateway.processPayment(pokemon.price * quantity * 100)
			.then(body => {
				if (body.status == 'paid') {
					pokemon.stock = pokemon.stock - quantity;
					return pokemon.save()
						.then(pokemon => body)
				}
			})
			.catch(err => {
				console.log(JSON.stringify(err, null, 4));
				return { statusCode: err.response.statusCode, body: err.response.body }
			}),
	}
}

module.exports = Store