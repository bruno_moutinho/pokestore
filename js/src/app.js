const React = require('react')
const ReactDOM = require('react-dom')

import Home from './screens/home'

ReactDOM.render(
	<Home />,
	document.getElementById('pokestore')
)