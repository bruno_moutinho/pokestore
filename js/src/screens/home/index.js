import React from 'react'

import Header from '../../components/header'
import PokeList from '../pokemon-list'
import PokemonDetail from '../pokemon-detail'

require('./index.css')

export default class Home extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			pokeList: [],
			selected: undefined,
			cart: {},
		}
	}

	componentWillMount() {
		fetch('/pokemon')
			.then(response => response.json())
			.then(pokeList => this.setState({ pokeList }))
	}

	buy() {
		const cart = Object.keys(this.state.cart).map(poke => {
			const pokemon = this.state.pokeList.filter(pokemon => pokemon.number == poke)[0]
			return {
				name: pokemon.name,
				quantity: this.state.cart[poke]
			}
		})
		const body = JSON.stringify({
			cart
		})
		const headers = new Headers()
		headers.append('Content-Type', 'application/json')
		fetch('/store/pokemon', {
			method: 'POST',
			body,
			headers
		})
			.then(response => console.log(response))
	}

	goToList() {
		console.log('goToList')
		this.setState({ selected: undefined })
	}

	selectPokemon(number) {
		return event => {
			this.setState({selected: number})
		}
	}

	addToCart({ pokemon, quantity }) {
		let { cart } = Object.assign( {}, this.state )
		cart[pokemon] = cart[pokemon] ? parseInt(cart[pokemon]) + parseInt(quantity) : parseInt(quantity)
		this.setState({ cart })
	}

	getContent() {
		if (this.state.selected > 0) {
			return (
				<PokemonDetail
						addToCart={ this.addToCart.bind(this) }
						pokemon={ this.state.pokeList.filter(pokemon => pokemon.number === this.state.selected)[0] }
				/>
			)
		} else {
			return (
				<PokeList
						pokemon={ this.state.pokeList }
						selectPokemon={ this.selectPokemon.bind(this) }
				/>
			)
		}
	}

	render() {
		return (
			<div className="home">
				<Header
						buy={ this.buy.bind(this) }
						cart={ this.state.cart }
						goToList={ this.goToList.bind(this) }
						pokeList={ this.state.pokeList }
						selected={ this.state.selected }
				/>
				<div className="content">
					{ this.getContent() }
				</div>
				<div className="footer">
					<h2>Footer</h2>
				</div>
			</div>
		)
	}
}