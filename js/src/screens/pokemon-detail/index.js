import React from 'react'

import { formatPrice } from '../../utils/formatters'

require('./index.css')

export default class PokemonDetail extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			quantity: 0,
		}
	}

	onChangeQuantity(event) {
		this.setState({ quantity: event.target.value })
	}

	addToCart() {
		if (this.state.quantity > 0) {
			this.props.addToCart({ pokemon: this.props.pokemon.number, quantity: this.state.quantity })
		}
	}

	render () {
		return (
			<div className={ 'pokemon' }>
				<h2>
					{ this.props.pokemon.name }
				</h2>
				<div className={ 'pokemon__details' }>
					{ `Number: ${this.props.pokemon.number}` }
					<br />
					{ `Price: ${formatPrice(this.props.pokemon.price)}` }
					<br />
					{ `Stock: ${this.props.pokemon.stock} units` }
				</div>
				<div className={ 'pokemon__actions' }>
					<input
							min='0'
							name='quantity'
							onChange={ this.onChangeQuantity.bind(this) }
							placeholder='Quantity'
							type='number'
							value={ this.state.quantity }
					/>
					<button
						onClick={ this.addToCart.bind(this) }
						title='add to cart'
					>
						<i className='fas fa-cart-plus'></i>
					</button>
				</div>
			</div>
		)
	}
}
