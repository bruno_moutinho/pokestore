import React from 'react'

import Pokemon from '../../components/pokemon'

require('./index.css')

const PokemonList = ({ selectPokemon, pokemon }) => {
	const pokeList = pokemon.map(poke => (
			<Pokemon
					key={ poke.number }
					onClick={ selectPokemon }
					pokemon={ poke }
			/>
		))
	return (
		<div className="pokemon-list">
			{ pokeList }
		</div>
	)
}

export default PokemonList