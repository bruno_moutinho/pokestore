export const formatPrice = price => `$ ${parseFloatFixed(price)}`

export const parseFloatFixed = value => `${(Math.round(value * 100) / 100).toFixed(2)}`
