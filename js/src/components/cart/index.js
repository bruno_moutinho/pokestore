import React from 'react'

import { formatPrice, parseFloatFixed } from '../../utils/formatters'

require('./index.css')

const Cart = ({ buy, cart, pokeList }) => {
	const cartDetails = Object.keys(cart).map(pokemon => {
		const pokemonDetails = pokeList.filter(poke => poke.number == pokemon)[0]
		return (
			<section
					className='cart__pokemon'
					key={ pokemon }
			>
				<span className='cart__pokemon__details'><i>{ `${pokemonDetails.name}: ${cart[pokemon]}`}</i></span>
				<span className='cart__pokemon__price'>{`${formatPrice(parseFloatFixed(pokemonDetails.price) * parseFloatFixed(cart[pokemon]))}`}</span>
			</section>
		)
	})
	const subtotal = Object.keys(cart).map(pokemon => {
		const pokemonDetails = pokeList.filter(poke => poke.number == pokemon)[0]
		return parseFloatFixed(pokemonDetails.price) * parseFloatFixed(cart[pokemon])
	}).reduce((acc, price) => acc + price, 0)
	return (
		<div className={ 'cart' }>
			<h3>Cart</h3>
			{ cartDetails }
			<section className={ 'cart__subtotal' }>
				<span className={ 'cart__subtotal__text' }>{ `Subtotal:` }</span>
				<span className={ 'cart__subtotal__price' }>{ `${formatPrice(subtotal)}` }</span>
			</section>
			<button
					onClick={ buy }
			>
				{ `Checkout` }
			</button>
		</div>
	)
}

export default Cart