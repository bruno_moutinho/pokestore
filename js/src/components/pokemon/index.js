import React from 'react'

import { formatPrice } from '../../utils/formatters'

require('./index.css')

const Pokemon = ({ onClick, pokemon }) => {
	return (
		<div className="pokemon" onClick={ onClick(pokemon.number) }>
			<p className="pokemon__name">{ `${pokemon.name}` }</p>
			<p className="pokemon__price">{ `${formatPrice(pokemon.price)}` }</p>
			<p className="pokemon__stock">{ `${pokemon.stock} in stock` }</p>
		</div>
	)
}

export default Pokemon