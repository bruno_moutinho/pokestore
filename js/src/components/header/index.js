import React from 'react'

import Cart from '../cart'

require('./index.css')

const Header = ({ buy, cart, goToList, pokeList, selected }) => {
	return (
		<div className='header'>
			<h1 className={ 'header__title' }>Welcome to the <i>Pokéstore!</i></h1>
			<div className={ 'header__content' }>
				{ selected &&
					<section
							className={ 'header__content__back' }
							onClick={ goToList }
					>
						<i className='fas fa-arrow-left'/>
					</section>
				}
			</div>
			<div className={ 'header__cart' }>
				<Cart
						buy={ buy }
						cart={ cart }
						pokeList={ pokeList }
				/>
			</div>
		</div>
	)
}

export default Header