const request = require('request-promise')
const { mockedCard } = require('../mocked_data')

const PaymentGateway = () => {
	const transactionsAPI = 'https://api.pagar.me/1/transactions'
	const api_key =  "ak_test_WHgSu2XFmvoopAZMetV3LfA2RfEEQg"

	return {
		processPayment: (amount, metadata) => {
			return request({
				uri: transactionsAPI,
				method: 'POST',
				json: {
					api_key,
					amount,
					metadata,
					...mockedCard
				}
			})
		}
	}
}

module.exports = PaymentGateway