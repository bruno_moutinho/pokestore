var express = require('express');
var app = express();
var bodyParser = require('body-parser');

const Pokemon = require('./models/pokemon-model');
const routes = require('./routes');

app.use(bodyParser.json());

app.listen(3000, function () {
	console.log('Listening on http://localhost:3000');
});

Pokemon.sync({force: true}).then(function () {
	console.log('Model is ready!');
});

app.use('/', routes)